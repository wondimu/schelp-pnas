## Analysis of demand profiles with nonlinear regression

### Introduction

A key tenet of economics is that as the price of a commodity increases, the demand for it goes down. In this paper we investigated how an individual responds to changing price. We trained mice to pay for a reward (sugar/sucrose) by pressing a lever. We then varied the unit price - the number of lever presses required to receive 1 mg of sucrose - and measured the amount consumed at each price point. We fit the experimentally observed demand profiles to a mathematical model to estimate the price sensitivity of demand. The following plot shows a typical **demand curve** from such an experiment (filled circles are observations, and the smooth demand curve is the estimated fit). This document explains the R code used for this analysis.

![single demand curve](/figures/one-demand-curve.png)

---

### Data

The data from all our experiments is compiled into a single file called <a href = "/data/optogenetics-data-master.csv" target = "_blank"> `optogenetics-data-master.csv` </a>. The following table displays the first few rows of this dataset:

| Condition | Task | Treatment | ID | Date | Replicate | Price | Consumption |
| :----- | ---- | -------- | --- | ---------- | - | :---- | --: |
| VTA WT | Cost | Baseline | 002 | 2015-04-14 | 1 | 0.022 | 900 |
| VTA WT | Cost | Baseline | 002 | 2015-04-14 | 1 | 0.067 | 810 |
| VTA WT | Cost | Baseline | 002 | 2015-04-14 | 1 | 0.133 | 810 |
| VTA WT | Cost | Baseline | 002 | 2015-04-14 | 1 | 0.222 | 810 |
| VTA WT | Cost | Baseline | 002 | 2015-04-14 | 1 | 0.4 | 720 |
| VTA WT | Cost | Baseline | 002 | 2015-04-14 | 1 | 0.68 | 540 |
| VTA WT | Cost | Baseline | 002 | 2015-04-14 | 1 | 1.244 | 315 |
| VTA WT | Cost | Baseline | 002 | 2015-04-14 | 1 | 2.222 | 90 |
| VTA WT | Cost | Baseline | 002 | 2015-04-14 | 1 | 4.0 | 0 |
| VTA WT | Cost | Baseline | 002 | 2015-04-14 | 1 | 6.889 | 0 |
| VTA WT | Cost | Baseline | 002 | 2015-04-15 | 2 | 0.022 | 900 |
| VTA WT | Cost | Baseline | 002 | 2015-04-15 | 2 | 0.067 | 855 |
| VTA WT | Cost | Baseline | 002 | 2015-04-15 | 2 | 0.133 | 810 |
| VTA WT | Cost | Baseline | 002 | 2015-04-15 | 2 | 0.222 | 765 |
| $`\vdots`$ | $`\vdots`$  | $`\vdots`$ | $`\vdots`$ | $`\vdots`$ | $`\vdots`$  | $`\vdots`$ | $`\vdots`$


This dataset contains the following variables. Also see the paper for further explanation of the study design:

- `Condition`: A combination of the stimulation region in the brain and the genetic background of the animal
    - VTA = ventral tagmental area
    - NAcc = Nucleus Accumbens
    - WT = wild type
    - Cre = expressing channelrhodopsin-2 in dopamine neurons 
- `Task`: Whether the price was altered by manipulating the number of lever presses required (Cost) or the amount of sucrose delivered (Reward)  
- `Treatment`: Optogenetic manipulation used. 
    - Baseline = No optical stimulation
    - Baseline repeat = No optical stimulation to re-establish baseline behavior
    - Cue laser = Optical Stimulation at Cue presentation
    - Reward laser = Optical stimulation at reward delivery).  
- `ID`: Animal ID  
- `Date`: Date of the experiment  
- `Replicate`: Each animal was subject to each treatment 3 times. This is the replicate number for that condition.   
- `Price`: The unit price measured in lever presses required per mg of sucrose. Each unit price was valid for a fixed epoch of time. This is the explanatory variable. 
- `Consumption`: mg of sugar consumed at a given unit price, or the reponse variable. 

We organize our workflow around the concept of <a href = "http://vita.had.co.nz/papers/tidy-data.html" target = "_blank"> tidy data </a>. In our dataset, each unique combination of the indexing variables: `Condition`, `Task`, `Treatment` `ID` and `Replicate` identifies a single demand profile. We use these indexing variables and the powerful <a href = "http://dplyr.tidyverse.org/" target = "_blank">dplyr package</a> to create an efficient data-fitting workflow described next.

### Nonlinear regression

The complete R code for this analysis is available in the file <a href = "/R/nonlinfit-all-expts.R" target = "_blank"> nonlinfit-all-expts.R </a>. Here we briefly explain each step. 

1. **Load required packages and import data**

    ```R
    # Load libraries
    library(broom)
    library(reshape2)
    library(magrittr)
    library(dplyr)
    library(minpack.lm)
    
    # Import data
    all.data = read.csv("data/optogenetics-data-master.csv", header=T,
                        colClasses = c(rep("factor", 4), "Date", "factor", rep("numeric", 2)))
    ```

2. **Fit demand profiles** We fit each demand profile to the following mathematical model:  
    ```math
    Q = Q_{\text{min}} + (Q_{\text{max}} - Q_{\text{min}}) e^{-\alpha C}
    ```
    where $`Q`$ is the consumption at a unit price $`C`$. The model has three undetermined parameters  
    - $`Q_{\text{max}}`$ - the maximal consumption when the unit price is 0.  
    - $`Q_{\text{min}}`$ - the asymptotic minimal consumption (as $`C \to \infty`$).  
    - $`\alpha`$ - the rate of decay of consumption 

    In this model, the consumption decays exponentially with unit price at a rate $`\alpha`$. We use the rate of decay as a measure of price sensitivity. The following code block splits the complete dataset into individual experiments, and fits individual demand profiles to estimate the model parameters. We use the function `nlsLM` from the <a href = "https://cran.r-project.org/web/packages/minpack.lm/index.html" target = "_blank"> `minpack.lm` </a> package to perform the nonlinear regressions, and save the output into the variable `fit.results`.
```R
    # Define model equation: Consumption = Q_min + (Q_max - Q_min)*exp(-alpha*Price)
    model = as.formula(Consumption ~ Q.min + (Q.max - Q.min)*exp(-alpha*Price)) 
    # Fit to model
    fit.results = all.data %>%
      group_by(Condition, Task, Treatment, ID, Date, Replicate) %>% # Group data by expt
      do(fit = nlsLM(model, data=.,
                     start = list(Q.max = max(.$Consumption),
                                  Q.min = 0.01,
                                  alpha = 0.1),
                     lower = c(0, 0, 0),
                     control = list(maxiter=200))) # Perform nonlinear regression
    ```

3. **Extract fit parameters** From the nonlinear regression output, we extract the parameter estimates and compute the sum of squared residuals:  
    ```R
    # Compute SSR for each fit
    fit.results %<>%
    	rowwise() %>%
    	mutate(ssfit = with(fit, sum((m$resid())^2)))
    
    # Extract best-fit parameter estimates and convert to tidy data frame
    par.estimates = fit.results %>%
      tidy(fit) %>% 
      dcast(Condition + Task + Treatment + ID + Date + Replicate + ssfit ~ term,
            value.var = "estimate")
    ```

4. **Compute R^2** To assess the quality of fits, we compute the R^2 statistic (R^2 = 1 - SSR/SS<sub>total</sub>)  
    ```R
    # Compute total sum of squares (needed for computing R^2)
    sstotal = all.data %>%
      group_by(Condition, Task, Treatment, ID, Date, Replicate) %>%
      summarise(sstot = sum((Consumption - mean(Consumption))^2) )
    
    # Compute R^2
    par.estimates %<>% 
    	left_join(sstotal) %>% 
    	mutate(R2 = 1 - ssfit/sstot) %>%
    	select(-ssfit, -sstot)
    ```

5. **Save results** Finally, save the results in this file <a href = "/results/optogenetics-single-expt-fits.csv" target = "_blank"> `optogenetics-single-expt-fits.csv` </a>  
    ```R
    write.csv(par.estimates, file="results/optogenetics-single-expt-fits.csv", row.names=F)
    ```  
    Here is an excerpt of this file:  

    | Condition | Task | Treatment | ID | Date | Replicate | alpha | Q.max | Q.min | R2 |
    | --------- | ---- | --------- | -- | ---- | --------- | ----- | ----- | ----- | -- |
    | $`\vdots`$ | $`\vdots`$  | $`\vdots`$ | $`\vdots`$ | $`\vdots`$ | $`\vdots`$  | $`\vdots`$ | $`\vdots`$ | $`\vdots`$ | $`\vdots`$ |
    | VTA WT | Cost | Baseline | 002 | 2015-04-14 | 1 | 0.828074045703217 | 923.45507854644 | 0 | 0.988234724522598 |
    | VTA WT | Cost | Baseline | 002 | 2015-04-15 | 2 | 0.751368367904049 | 919.422475165853 | 0 | 0.98762114604277 |
    | VTA WT | Cost | Baseline | 002 | 2015-04-16 | 3 | 0.758450881826002 | 905.377061731358 | 0 | 0.983608734062712 |
    | $`\vdots`$ | $`\vdots`$  | $`\vdots`$ | $`\vdots`$ | $`\vdots`$ | $`\vdots`$  | $`\vdots`$ | $`\vdots`$ |  $`\vdots`$ | $`\vdots`$ |
    
### Visualizations

The following plots summarize our findings:

- We used optogenetics to augment the dopamine levels in the animals' brains during the task (i.e. artificially induce DA neurons to fire, and release a pulse of DA). We found that the timing of the DA augmentation altered price sensitivity: augmenting DA at cue (prior to reward delivery) increases price sensitivity, while augmenting DA at reward delivery decreases price sensitivity. The following demand curves obtained from a single animal illustrates this:  
    ![Effect of optogenetic stimulation on demand curves for one animal](/figures/one-animal-demand-curves.png)  
    The change in price sensitivity manifests in the shape of the demand profile. The baseline curve is the baseline behavior in the absence of any optical stimulation. Stimulation at cue shifts the demand profile to the left, meaning it decays faster. In other words the demand is more sensitive to price. In contrast, stimulation at reward delivery leads to a more persistent demand at higher prices. In other words, the demand is less sensitive to prive.  
    We can quantify the change in sensitivity by comparing the estimated $`\alpha`$ values for the three conditions. The estimated $`\alpha`$ for stimulation at cue is higher than the estimated baseline $`\alpha`$, consistent with the greater price sensitivity. In contrast, the estimated $`\alpha`$ for stimulation at reward is smaller than the baseline value, consistent with a lower price sensitivity. This trend is consistent in both the cost-manipulation task (top panel) and the reward-manipulation task (bottom panel)
    
- The above plot shows representative demand curves for a single animal. Do these trends persist when we consider individual variability?  
    Below we summarise the observed and fitted demand curves for multiple animals with 3 independent replicates for each animal. These data are for the cost-manipulation task. The left panel shows data from wildtype animals (WT control) that do not express the channelrhodopsin-2 receptor, and are therefore unaffected by optical stimulation. The middle and right panel are data from ChR2-expressing animals that were optically stimulated in the ventral-tagmentum (VTA) or the Nucleus-accumbens (NAcc) regions of the brain:
    ![Cost-manipulation task demand curves](/figures/cost-task-demand-curves.png)  
    As expected, in WT animals the optical stimulation has no noticeable effect on the demand profiles. In contrast, ChR2-expressing animals show overall shifts in demand profiles in reponse to optical stimulation. As seen above, stimulation at cue increases price sensitivity relative to baseline, while stimulation at reward delivery decreases price sensitivity relative to baseline. We observe a similar trend for the reward-manipulation task:  
    ![Reward-manipulation task demand curves](/figures/reward-task-demand-curves.png)  

- Finally, to quantify these changes in price sensitivity, we look at the distribution of estimated $`\alpha`$ values for each condition:  
    ![Estimated alpha values from nonlinear regression](/figures/alpha-estimates.png)  
    We find that in WT animals, the distribution of estimated $`\alpha`$ values is essentially unchanged in response to the optogenetic manipulation. In contrast, in ChR2-expressing animals, augmenting DA release at cue increases $`\alpha`$, while augmenting DA release at reward delivery decreases $`\alpha`$. These results are consistent with the observed shifts in demand profiles seen in the previous set of plots. 
    
---


